package com.eg.atestapp.model.entities

data class User (
    val username : String,
    val level : Int,
    val experience : Int,
    val experienceRequiredForNextLevel : Int)