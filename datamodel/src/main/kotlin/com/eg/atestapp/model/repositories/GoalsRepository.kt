package com.eg.atestapp.model.repositories

import androidx.lifecycle.LiveData
import com.eg.atestapp.model.entities.Goal
import com.eg.atestapp.model.entities.GoalType
import com.eg.atestapp.model.entities.Resource

interface GoalsRepository {

    fun getGoals() : LiveData<Resource<List<Goal>>>
    fun updateGoalProgress(progress: Int, goalType: GoalType)
    fun getCompletedGoalsPoints() : Int
    fun getGoalsById(id : Int): LiveData<Resource<Goal>>

}