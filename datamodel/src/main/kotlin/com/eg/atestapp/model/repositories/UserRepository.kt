package com.eg.atestapp.model.repositories

import androidx.lifecycle.LiveData
import com.eg.atestapp.model.entities.Resource
import com.eg.atestapp.model.entities.User

interface UserRepository {

    fun getUser() : LiveData<User>
    fun setUserName(userName : String)
    fun updateExperience(totalExperience: Int)
}