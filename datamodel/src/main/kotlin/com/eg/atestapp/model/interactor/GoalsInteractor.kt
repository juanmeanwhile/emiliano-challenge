package com.eg.atestapp.model.interactor

import androidx.lifecycle.LiveData
import com.eg.atestapp.model.entities.Goal
import com.eg.atestapp.model.entities.GoalType
import com.eg.atestapp.model.entities.Resource
import com.eg.atestapp.model.repositories.GoalsRepository
import com.eg.atestapp.model.repositories.UserRepository
import javax.inject.Inject

class GoalsInteractor @Inject constructor(private val userRepository: UserRepository, private val goalsRepository: GoalsRepository) {

    fun updateGoalStatus(progress: Int, goalType: GoalType) {
        goalsRepository.updateGoalProgress(progress, goalType)
        updateUserExperience()
    }

    fun getGoals(): LiveData<Resource<List<Goal>>> {
        return goalsRepository.getGoals()
    }

    private fun updateUserExperience(){
        val totalExperience = goalsRepository.getCompletedGoalsPoints()  //TODO blocking call!
        userRepository.updateExperience(totalExperience)
    }

    fun getGoalByID(goalID: Int) : LiveData<Resource<Goal>>{
        return goalsRepository.getGoalsById(goalID)
    }

}