package com.eg.atestapp.model.util

/**
Calculates the user level based on the experience granted by goals
 */
class LevelCalculator {

    companion object {
        // This is a simple solution just for the demo
        private const val LVL0 = 0
        private const val LVL1 = 1
        private const val REQUIRED_EXP_LVL1 = 5
        private const val LVL2 = 2
        private const val REQUIRED_EXP_LVL2 = 15
        private const val LVL3 = 3
        private const val REQUIRED_EXP_LVL3 = 30
        private const val LVL4 = 4
        private const val REQUIRED_EXP_LVL4 = 45
        private const val LVL5 = 5
        private const val REQUIRED_EXP_LVL5 = 88
    }

    fun calculateLevel(experience: Int): Int {
        return when (experience) {
            in 0 until REQUIRED_EXP_LVL1 -> LVL0
            in REQUIRED_EXP_LVL1 until REQUIRED_EXP_LVL2 -> LVL1
            in REQUIRED_EXP_LVL2 until REQUIRED_EXP_LVL3 -> LVL2
            in REQUIRED_EXP_LVL3 until REQUIRED_EXP_LVL4 -> LVL3
            in REQUIRED_EXP_LVL4 until REQUIRED_EXP_LVL5 -> LVL4
            else -> LVL5
        }
    }

    fun requiredForNextLevel(userExperience: Int): Int {
        return when (calculateLevel(userExperience)) {
            LVL0 -> REQUIRED_EXP_LVL1
            LVL1 -> REQUIRED_EXP_LVL2
            LVL2 -> REQUIRED_EXP_LVL3
            LVL3 -> REQUIRED_EXP_LVL4
            LVL4 -> REQUIRED_EXP_LVL5
            else -> REQUIRED_EXP_LVL5
        }
    }
}