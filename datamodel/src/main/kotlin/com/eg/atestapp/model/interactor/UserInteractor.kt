package com.eg.atestapp.model.interactor

import androidx.lifecycle.LiveData
import com.eg.atestapp.model.entities.User
import com.eg.atestapp.model.repositories.UserRepository
import javax.inject.Inject

class UserInteractor @Inject constructor(private val userRepository: UserRepository) {

    fun setUserName(username : String){
        userRepository.setUserName(username)
    }

    fun getUser(): LiveData<User> {
        return userRepository.getUser()
    }

}