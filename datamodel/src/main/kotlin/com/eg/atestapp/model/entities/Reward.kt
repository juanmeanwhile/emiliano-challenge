package com.eg.atestapp.model.entities

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Reward (
    val trophy: String,
    val points: Int)
    : Parcelable