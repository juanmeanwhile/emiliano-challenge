package com.eg.atestapp.model.injection

import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ModelModule::class])
interface ModelComponent