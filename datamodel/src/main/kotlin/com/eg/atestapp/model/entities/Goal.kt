package com.eg.atestapp.model.entities

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Goal(
    val id: Int,
    val title: String,
    val description: String,
    val type: GoalType,
    val progress: Int,
    val goal: Int,
    val reward: Reward
) : Parcelable {
    fun isComplete() = progress > goal
}