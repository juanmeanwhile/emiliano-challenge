package com.eg.atestapp.model.entities

enum class GoalType {
    STEP,
    RUNNING_DISTANCE,
    WALKING_DISTANCE,
    NONE
}