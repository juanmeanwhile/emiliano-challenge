package com.eg.atestapp.data.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.eg.atestapp.data.dao.GoalDatabase
import com.eg.atestapp.data.entities.JsonGoal
import com.eg.atestapp.data.entities.JsonGoalResponse
import com.eg.atestapp.data.executor.Executors
import com.eg.atestapp.data.repository.mappers.GoalMapper
import com.eg.atestapp.data.services.GoalsServiceAPI
import com.eg.atestapp.model.entities.Goal
import com.eg.atestapp.model.entities.GoalType
import com.eg.atestapp.model.entities.Resource
import com.eg.atestapp.model.entities.Status
import com.eg.atestapp.model.repositories.GoalsRepository
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.concurrent.TimeUnit
import javax.inject.Inject

//TODO Juan: good for injecting objects in the constructor
class GoalsRepositoryImp @Inject constructor(
    private val goalsDAO: GoalDatabase,
    private val goalsServiceAPI: GoalsServiceAPI,
    private val appExecutors: Executors
) : GoalsRepository {

    override fun getGoals(): LiveData<Resource<List<Goal>>> {

        var data = getGoalsFromDisk()

        appExecutors.NETWORK.execute {

            //TODO Juan: misterious callback inside executor
            goalsServiceAPI.getListOfGoals().enqueue(object :
                Callback<JsonGoalResponse> {

                override fun onFailure(call: Call<JsonGoalResponse>, t: Throwable) {
                    data.postValue(
                        Resource(
                            Status.ERROR,
                            null,
                            t.message
                        )
                    )
                    Log.e("Network Call Failed", t.toString())
                }

                override fun onResponse(call: Call<JsonGoalResponse>, response: Response<JsonGoalResponse>) {
                    if (response.isSuccessful) {
                        response.body()?.let {
                            data.postValue( // We could do this directly by saving a live data in the database. //TODO Juan explanation appreciated
                                Resource(
                                    Status.SUCCESS,
                                    GoalMapper.mapListOfGoals(it.items),
                                    response.message()
                                )
                            )
                            saveResultIntoDisk(it.items) //TODO Juan: a shame because iits not fully using the livedata capabilities
                        }
                    } else {
                        data.postValue(
                            Resource(
                                Status.ERROR,
                                null,
                                response.errorBody()!!.string()
                            )
                        )
                    }

                }

            })
        }
        return data
    }

    private fun saveResultIntoDisk(items: List<JsonGoal>) {
        //TODO Juan: switching from one thread of another
        appExecutors.DISKIO.execute {
            goalsDAO.daoAccess().insertGoals(items)
        }
    }

    private fun getGoalsFromDisk(): MutableLiveData<Resource<List<Goal>>> {
        var localData = MutableLiveData<Resource<List<Goal>>>()
        localData.postValue(Resource(Status.LOADING,null, null))

        //TODO Juan: This mapper makes no sense, he could get this info from db
        appExecutors.DISKIO.execute {
            val localGoals = GoalMapper.mapListOfGoals(goalsDAO.daoAccess().fetchAllGoals())
            if (localGoals.isNotEmpty()) {
                localData.postValue(
                    Resource(
                        Status.SUCCESS,
                        localGoals,
                        "Goals from disk"
                    )
                )
            }
        }
        return localData
    }

    /**
     *  Updates the goals that share the same type (ie. step, run, distance)
     */
    override fun updateGoalProgress(progress: Int, goalType: GoalType) {
        appExecutors.DISKIO.execute {
            //TODO Juan: fetching all the goals when he could be updating via a simple update with WHERE
            val jsonGoals = goalsDAO.daoAccess().fetchAllGoals()
            if (jsonGoals.isNotEmpty()) {
                for (jsonGoal in jsonGoals) {
                    if (goalType == GoalMapper.mapType(jsonGoal.type)) {
                        checkGoalProgress(progress, jsonGoal)
                    }
                }
            }
        }
    }

    /**
     *  Updates the progress only if its greater than previous one.
     */
    private fun checkGoalProgress(progress: Int, jsonGoal: JsonGoal) {
        if (progress > jsonGoal.progress) {
            val goalUpdated = JsonGoal(
                jsonGoal.id, jsonGoal.title, jsonGoal.description,
                jsonGoal.type, progress, jsonGoal.goal, jsonGoal.trophy
            )
            goalsDAO.daoAccess().updateGoal(goalUpdated)
        }
    }

    /**
     *  Returns the summatory of all the completed goals points.
     */
    //TODO Juan: This is a blocking operation, very bad idea, might throw an exception by inactivity
    override fun getCompletedGoalsPoints(): Int {
        var totalPoints = 0
        appExecutors.DISKIO.execute {
            val jsonGoals = goalsDAO.daoAccess().fetchCompletedGoals()
            for (jsonGoal in jsonGoals) {
                totalPoints += jsonGoal.trophy.points
            }
        }
        appExecutors.DISKIO.awaitTermination(1, TimeUnit.SECONDS)
        return totalPoints
    }

    override fun getGoalsById(id : Int): LiveData<Resource<Goal>> {
        var localData = MutableLiveData<Resource<Goal>>()
        localData.postValue(Resource(Status.LOADING,null, null))

        appExecutors.DISKIO.execute {
            val localGoals = GoalMapper.mapToGoal(goalsDAO.daoAccess().fetchGoalById(id))
                localData.postValue(
                    Resource(
                        Status.SUCCESS,
                        localGoals,
                        "Goal recovered from disk"
                    )
                )
        }
        return localData
    }
}