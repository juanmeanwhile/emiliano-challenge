package com.eg.atestapp.data.repository.mappers

import com.eg.atestapp.data.entities.JsonGoal
import com.eg.atestapp.data.entities.JsonTrophy
import com.eg.atestapp.model.entities.Goal
import com.eg.atestapp.model.entities.GoalType
import com.eg.atestapp.model.entities.Reward

class GoalMapper {

    companion object {
        //TODO Juan: Should use map
        fun mapListOfGoals(listJsonGoal : List<JsonGoal>): MutableList<Goal> {
            var mutableListGoal = mutableListOf<Goal>()
            for (jsonGoal in listJsonGoal) {
                mutableListGoal.add(mapToGoal(jsonGoal))
            }
            return mutableListGoal
        }


        //TODO Juan: this a relation in db which is doing manually
        fun mapToGoal(jsonGoal: JsonGoal) = Goal(
            jsonGoal.id, jsonGoal.title, jsonGoal.description, mapType(jsonGoal.type), jsonGoal.progress, jsonGoal.goal,
            Reward(jsonGoal.trophy.trophy, jsonGoal.trophy.points)
        )

        private fun mapToJsonGoal(goal: Goal) = JsonGoal (
            goal.id, goal.title, goal.description, mapType(goal.type), goal.progress, goal.goal,
            JsonTrophy(goal.reward.trophy, goal.reward.points)
        )

        fun mapType(type: String): GoalType {
            return when(type) {
                "step" -> GoalType.STEP
                "walking_distance" -> GoalType.WALKING_DISTANCE
                "running_distance" -> GoalType.RUNNING_DISTANCE
                else -> GoalType.NONE
            }
        }

        fun mapType(goalType: GoalType): String {
            return when(goalType) {
                GoalType.STEP -> "step"
                GoalType.WALKING_DISTANCE -> "walking_distance"
                GoalType.RUNNING_DISTANCE -> "running_distance"
                else -> ""
            }
        }
    }
}