package com.eg.atestapp.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.eg.atestapp.data.executor.Executors
import com.eg.atestapp.data.preferences.SharedPreferencesManager
import com.eg.atestapp.model.entities.Resource
import com.eg.atestapp.model.entities.Status
import com.eg.atestapp.model.entities.User
import com.eg.atestapp.model.repositories.UserRepository
import com.eg.atestapp.model.util.LevelCalculator
import javax.inject.Inject

class UserRepositoryImp @Inject constructor(
    private val sharedPreferencesManager: SharedPreferencesManager,
    private val levelCalculator: LevelCalculator,
    private val appExecutors: Executors)
        : UserRepository {

    override fun getUser(): LiveData<User> {
        val liveData = MutableLiveData<User>()

        appExecutors.DISKIO.execute {
            val username = sharedPreferencesManager.getUserName()
            val userExperience = sharedPreferencesManager.getUserExperience()
            // This kind of business logic should be moved into a use case or domain class.
            liveData.postValue(
                User(
                    username,
                    levelCalculator.calculateLevel(userExperience),
                    userExperience,
                    levelCalculator.requiredForNextLevel(userExperience)
                )
            )
        }
        return liveData
    }

    override fun setUserName(userName: String) {
        appExecutors.DISKIO.execute { //TODO Juan: extra care for accessing SharedPrefs, although I'm not sure if is done because he knews that accessing
            // shared its a heavy operation
            sharedPreferencesManager.setUserName(userName)
        }
    }


    override fun updateExperience(totalExperience: Int) {
        appExecutors.DISKIO.execute {
            sharedPreferencesManager.setUserExperience(totalExperience)
        }
    }


}