package com.eg.atestapp.data.executor

import java.util.concurrent.Executors

/*
 I do not favor constant classes, its just a demo work-around for thread usage.
 Still, we dont need more than DAO, NETWORK and MAIN THREAD for this demo.
 */

//TODO Juan: Nice
class Executors(){
    val DISKIO = Executors.newFixedThreadPool(2)
    val NETWORK = Executors.newFixedThreadPool(3)
    val BACKGROUND = Executors.newFixedThreadPool(4)
}

