package com.eg.atestapp.data.services

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class GoalsRetrofitRequest {

    companion object {

        val retrofit: Retrofit =  Retrofit.Builder()
                .baseUrl("https://thebigachallenge.appspot.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()

    }

}