package com.eg.atestapp.data.preferences

import android.content.SharedPreferences

//TODO: Juan good for embedding the class, I'd prefer to have another more semantic name but is personal taste
class SharedPreferencesManager(private val sharedPreferences: SharedPreferences) {

    companion object {
        // Can be defined in strings.xml, done this way just for demo  //TODO Juan: why do you need strings.xml for the prefs? no sense
        private const val USERNAME = "USERNAME"
        private const val USER_EXPERIENCE = "USER_EXPERIENCE"
    }

    fun getUserName() = sharedPreferences.getString(USERNAME, "")!!
    fun getUserExperience() = sharedPreferences.getInt(USER_EXPERIENCE, 0)

    fun setUserName(userName : String) {
        sharedPreferences.edit().putString(USERNAME, userName).apply()
    }

    fun setUserExperience(userExperience : Int) {
        sharedPreferences.edit().putInt(USER_EXPERIENCE, userExperience).apply()
    }

}