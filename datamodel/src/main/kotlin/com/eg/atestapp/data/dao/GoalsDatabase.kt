package com.eg.atestapp.data.dao

import androidx.room.Database
import androidx.room.RoomDatabase
import com.eg.atestapp.data.entities.JsonGoal

@Database(entities = [JsonGoal::class], version = 1, exportSchema = false)
abstract class GoalDatabase : RoomDatabase() {
    abstract fun daoAccess(): GoalsDaoAccess
}