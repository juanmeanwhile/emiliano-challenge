package com.eg.atestapp.data.dao

import androidx.room.*
import com.eg.atestapp.data.entities.JsonGoal

@Dao
interface GoalsDaoAccess {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertGoal(jsonGoal: JsonGoal)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertGoals(jsonGoals: List<JsonGoal>)

    @Query("SELECT * FROM JsonGoal WHERE id = :goalID")
    fun fetchGoalById(goalID: Int): JsonGoal

    @Query("SELECT * FROM JsonGoal")
    fun fetchAllGoals(): List<JsonGoal>

    @Query("SELECT * FROM JsonGoal WHERE progress >= goal")
    fun fetchCompletedGoals(): List<JsonGoal>

    @Update
    fun updateGoal(jsonGoal: JsonGoal)

    @Delete
    fun deleteGoal(jsonGoal : JsonGoal)
}