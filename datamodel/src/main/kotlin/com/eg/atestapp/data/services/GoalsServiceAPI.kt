package com.eg.atestapp.data.services

import com.eg.atestapp.data.entities.JsonGoalResponse
import retrofit2.Call
import retrofit2.http.GET

interface GoalsServiceAPI {

    @GET("_ah/api/myApi/v1/goals")
    fun getListOfGoals()
            : Call<JsonGoalResponse>
}