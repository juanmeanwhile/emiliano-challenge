package com.eg.atestapp.data.injection

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import androidx.room.Room
import com.eg.atestapp.data.dao.GoalDatabase
import com.eg.atestapp.data.executor.Executors
import com.eg.atestapp.data.preferences.SharedPreferencesManager
import com.eg.atestapp.data.repository.GoalsRepositoryImp
import com.eg.atestapp.data.repository.UserRepositoryImp
import com.eg.atestapp.data.services.GoalsRetrofitRequest
import com.eg.atestapp.data.services.GoalsServiceAPI
import com.eg.atestapp.model.repositories.GoalsRepository
import com.eg.atestapp.model.repositories.UserRepository
import com.eg.atestapp.model.util.LevelCalculator
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 * Module for the data layer (also domain in this case)
 * Should be splitted in two.
 */

@Module
class DataModule(internal val application : Application) {

    @Provides
    @Singleton
    fun provideAppExecutors(): Executors {
        return Executors()
    }

    @Provides
    fun provideUserRepository(): UserRepository {
        return UserRepositoryImp(providePreferencesManager(), provideLevelCalculator(), provideAppExecutors())
    }

    @Provides
    fun provideGoalsRepository(): GoalsRepository {
        return GoalsRepositoryImp(provideGoalsDatabase(), provideGoalsService(), provideAppExecutors())
    }

    @Provides
    fun provideGoalsService(): GoalsServiceAPI {
        return provideRetrofitClient().create(GoalsServiceAPI::class.java)
    }

    @Provides
    @Singleton
    fun provideRetrofitClient(): Retrofit {
        return GoalsRetrofitRequest.retrofit
    }

    @Provides
    fun provideGoalsDatabase() : GoalDatabase {
        return Room.databaseBuilder(
            application.baseContext,
            GoalDatabase::class.java,
            application.packageName + "_goals" // Database Name should be defined in another file
        ).fallbackToDestructiveMigration().build() // We dont need to migrate in this demo app
    }

    @Provides
    @Singleton
    fun providePreferencesManager(): SharedPreferencesManager {
        return SharedPreferencesManager(providePreferences())
    }

    @Provides
    @Singleton
    fun providePreferences(): SharedPreferences {
        return application.getSharedPreferences("REPOSITORY_PREFERENCES",
            Context.MODE_PRIVATE)
    }

    @Provides
    @Singleton
    fun provideLevelCalculator(): LevelCalculator {
        return LevelCalculator()
    }
}