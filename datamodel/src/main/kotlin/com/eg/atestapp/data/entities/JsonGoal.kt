package com.eg.atestapp.data.entities

import android.os.Parcelable
import androidx.annotation.NonNull
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity
data class JsonGoal(  //TODO Juan: Name as Json something that goes to DB is not my fully correct, although I understand he is reusing network and db model
    @PrimaryKey @NonNull
    @SerializedName("id") val id: Int,
    @SerializedName("title") val title: String,
    @SerializedName("description") val description: String,
    @SerializedName("type") val type: String,
    @SerializedName("progress") val progress: Int,
    @SerializedName("goal") val goal: Int,
    @SerializedName("reward") @Embedded val trophy : JsonTrophy  //TODO this should be a relation, although embedded could be OK for this demo.
) : Parcelable
