package com.eg.atestapp.data.entities

import android.os.Parcelable
import androidx.room.Entity
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity
data class JsonTrophy(
    @SerializedName("trophy") val trophy: String,
    @SerializedName("points") val points: Int) : Parcelable
