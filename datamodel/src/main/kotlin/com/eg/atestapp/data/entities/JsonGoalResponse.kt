package com.eg.atestapp.data.entities

import android.os.Parcelable
import androidx.annotation.NonNull
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity
data class JsonGoalResponse(
    @PrimaryKey @NonNull
    @SerializedName("items") val items: List<JsonGoal>,
    @SerializedName("nextPageToken") val nextPage: String
) : Parcelable
