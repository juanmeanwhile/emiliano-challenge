package com.eg.atestapp.model.entities

import org.junit.Test

class GoalTest {

    @Test
    fun testIsComplete() {
        val goal = Goal(1, "Run!", "Run a lot", GoalType.RUNNING_DISTANCE, 7, 5, Reward("monkey runner", 2))
        assert(goal.isComplete())
    }

    @Test
    fun testIsNotComplete(){
        val goal = Goal(1, "Run!", "Run a lot", GoalType.RUNNING_DISTANCE, 1, 5, Reward("monkey runner", 2))
        assert(goal.isComplete().not())
    }
}