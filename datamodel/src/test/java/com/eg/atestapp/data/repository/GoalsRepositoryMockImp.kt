package com.eg.atestapp.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.eg.atestapp.model.entities.*
import com.eg.atestapp.model.repositories.GoalsRepository

/**
 *  Used as mock repository in fake build. To be used in tests.
 */

//TODO Juan: +1 for the test
class GoalsRepositoryMockImp : GoalsRepository {

    var fakeGoals = setFakeGoals()

    fun setFakeGoals() = mutableListOf<Goal>().apply {
        add(Goal(1, "Run!", "Run a lot", GoalType.RUNNING_DISTANCE, 1, 5, Reward("monkey runner", 2)))
        add(Goal(2, "Walk!", "Walk a lot", GoalType.WALKING_DISTANCE, 3, 10, Reward("monkey runner", 1)))
        add(Goal(2, "Walk!", "Walk a lot", GoalType.STEP, 15, 10, Reward("monkey walker", 20)))
    }

    override fun updateGoalProgress(progress: Int, goalType: GoalType) {
        fakeGoals.map { goal -> updateGoal(goal, progress, goalType) }
    }

    private fun updateGoal(goal: Goal, progress: Int, goalType: GoalType): Goal {
        return if (goal.type == goalType) {
            Goal(goal.id, goal.title, goal.description, goal.type, progress, goal.goal, goal.reward)
        } else {
            goal
        }
    }

    override fun getCompletedGoalsPoints(): Int {
        return fakeGoals.filter { goal ->  goal.isComplete() }.sumBy { goal -> goal.reward.points }
    }

    override fun getGoalsById(id: Int): LiveData<Resource<Goal>> {
        var mutableFakeUserLiveData = MutableLiveData<Resource<Goal>>()
        var resource: Resource<Goal> = Resource(Status.LOADING, null, "Downloading Goals")

        val goal =  fakeGoals.find { goal -> goal.id == id }
        goal?.let {
            mutableFakeUserLiveData.value =  Resource(Status.LOADING, it, "Downloading Goals")
        }
        mutableFakeUserLiveData.value = resource
        return mutableFakeUserLiveData
    }

    override fun getGoals(): LiveData<Resource<List<Goal>>> {
        var mutableFakeUserLiveData = MutableLiveData<Resource<List<Goal>>>()

        var resource = Resource(Status.SUCCESS, fakeGoals, "Goals Downloaded")
        mutableFakeUserLiveData.value = resource

        return mutableFakeUserLiveData
    }

}