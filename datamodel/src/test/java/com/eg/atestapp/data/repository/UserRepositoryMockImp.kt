package com.eg.atestapp.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.eg.atestapp.model.entities.User
import com.eg.atestapp.model.repositories.UserRepository
import com.eg.atestapp.model.util.LevelCalculator
import javax.inject.Inject

/**
 *  Used as mock repository in fake build. To be used in tests.
 */

class UserRepositoryMockImp : UserRepository {

    private var levelCalculator = LevelCalculator()

    var mutableFakeUserLiveData = MutableLiveData<User>().apply {
        value = setUser()
    }

    fun setUser(): User {
        return User(
            "Emi", levelCalculator.calculateLevel(0), 0, levelCalculator.calculateLevel(0)
        )
    }

    override fun getUser(): LiveData<User> {
        return mutableFakeUserLiveData
    }

    override fun setUserName(userName: String) {
        val olduser = mutableFakeUserLiveData.value!!
        val newUser = User(userName, olduser.level, olduser.experience, olduser.experienceRequiredForNextLevel)
        mutableFakeUserLiveData.postValue(newUser)
    }

    override fun updateExperience(totalExperience: Int) {
        val olduser = mutableFakeUserLiveData.value!!
        val newUser = User(olduser.username, levelCalculator.calculateLevel(totalExperience),
            totalExperience, levelCalculator.calculateLevel(totalExperience))

        mutableFakeUserLiveData.postValue(newUser)
    }
}