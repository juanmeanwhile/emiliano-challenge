package com.eg.atestapp.util

import androidx.lifecycle.LiveData
import com.eg.atestapp.model.entities.Resource

class AbsentLiveData<T : Any?> private constructor() : LiveData<T>() {
    init {
        postValue(null)
    }

    companion object {
        fun <T> create(): LiveData<Resource<T>> {
            return AbsentLiveData()
        }
    }
}