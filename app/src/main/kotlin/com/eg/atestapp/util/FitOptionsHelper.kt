package com.eg.atestapp.util

import com.google.android.gms.fitness.FitnessOptions
import com.google.android.gms.fitness.data.DataType

/**
 *  Class that defines the fit permissions to be asked on google's sign in process.
 */
class FitOptionsHelper {

    var options: FitnessOptions = FitnessOptions.builder()
        .addDataType(DataType.TYPE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_READ)
        .addDataType(DataType.TYPE_DISTANCE_DELTA, FitnessOptions.ACCESS_READ)
        .addDataType(DataType.AGGREGATE_ACTIVITY_SUMMARY, FitnessOptions.ACCESS_READ)
        .build()

}