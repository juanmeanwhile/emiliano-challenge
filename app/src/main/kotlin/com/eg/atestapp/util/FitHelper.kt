package com.eg.atestapp.util

import android.app.Activity
import android.content.ContentValues.TAG
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.eg.atestapp.model.entities.Resource
import com.eg.atestapp.model.entities.Status
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.fitness.Fitness
import com.google.android.gms.fitness.data.*
import com.google.android.gms.fitness.request.DataReadRequest
import com.google.android.gms.fitness.result.DataReadResponse
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList
import kotlin.math.roundToInt


class FitHelper {

    /**
     *  Ids provided in https://developers.google.com/fit/rest/v1/reference/activity-types
     */
    private fun runningActivitiesIds() = arrayListOf(8, 56, 57, 58)
    private fun walkingActivitiesIds() = arrayListOf(7, 93, 94 , 95, 116)

    /**
     *  Subscribes the app to record steps and distance
     *
     */
    fun subscribe(activity: Activity) {
        GoogleSignIn.getLastSignedInAccount(activity)!!.let {
            Fitness.getRecordingClient(activity, it).subscribe(DataType.TYPE_STEP_COUNT_CUMULATIVE)
                .addOnSuccessListener {
                    Log.d(TAG, "Successfully subscribed!")
                }
                .addOnFailureListener {
                    Log.w(TAG, "There was a problem subscribing.")
                }
            Fitness.getRecordingClient(activity, it).subscribe(DataType.TYPE_DISTANCE_DELTA)
                .addOnSuccessListener {
                    Log.d(TAG, "Successfully subscribed!")
                }
                .addOnFailureListener {
                    Log.w(TAG, "There was a problem subscribing.")
                }
        }
    }

    fun getTodaySteps(activity: Activity) : LiveData<Resource<Int>>{
        GoogleSignIn.getLastSignedInAccount(activity)!!.let { googleSignInAccount ->

            var todayStepsLiveData = MutableLiveData<Resource<Int>>()

            Fitness.getHistoryClient(activity, googleSignInAccount)
                .readDailyTotalFromLocalDevice(DataType.TYPE_STEP_COUNT_DELTA).addOnSuccessListener {
                    todayStepsLiveData.postValue(processStepsCallback(it))
                }

            return todayStepsLiveData
        }
    }

    private fun processStepsCallback(dataSet: DataSet): Resource<Int>? {
        var amountOfSteps = 0
        if(dataSet.dataPoints.isNotEmpty()){
            amountOfSteps = dataSet.dataPoints.first().getValue(Field.FIELD_STEPS).asInt()
        }
        return Resource(
            Status.SUCCESS,
            amountOfSteps,
            "Steps walked today $amountOfSteps"
        )
    }

    fun getTodayRunningDistance(activity: Activity) = getTodayDistance(activity, runningActivitiesIds())

    fun getTodayWalkingDistance(activity: Activity) = getTodayDistance(activity, walkingActivitiesIds())

    private fun getTodayDistance(activity: Activity, activityTypes: ArrayList<Int>) : LiveData<Resource<Int>>{
        GoogleSignIn.getLastSignedInAccount(activity)!!.let { googleSignInAccount ->

            var todayWalkingDistanceLiveData = MutableLiveData<Resource<Int>>()

            val readRequest = createAggregateDataRequest()

            Fitness.getHistoryClient(activity, googleSignInAccount)
                .readData(readRequest).addOnSuccessListener {
                    todayWalkingDistanceLiveData.postValue(processDistanceCallback(it, activityTypes))
                }

            return todayWalkingDistanceLiveData
        }
    }

    private fun processDistanceCallback(dataSet: DataReadResponse, activityTypes: ArrayList<Int>): Resource<Int>? {
        var distance = 0f

        Log.i("DATA", dataSet.toString())

        if (dataSet.buckets.isNotEmpty()) {
            val walkingDataBuckets = dataSet.buckets.filter { bucket -> activityTypes.contains( bucket.activityType ) }
            for (walkingDataBucket in walkingDataBuckets) {
                val dataset = walkingDataBucket.getDataSet(DataType.TYPE_DISTANCE_DELTA)
                dataset?.let {
                    distance = getLongestDistance(distance, it.dataPoints)
                }
            }
        }

        return Resource(
            Status.SUCCESS,
            distance.roundToInt(),
            "Distance moved today $distance"
        )
    }

    /**
     *  We keep the biggest distance in the last 24 hs
     */
    private fun getLongestDistance(distance: Float, datapoints: MutableList<DataPoint>): Float {
        var longestDistance = distance
        for (distanceDatapoint in datapoints) {
            val analyzedDistance = distanceDatapoint.getValue(Field.FIELD_DISTANCE).asFloat()
            if (longestDistance < analyzedDistance) {
                longestDistance = analyzedDistance
            }
        }
        return longestDistance
    }

    private fun createAggregateDataRequest() =
        DataReadRequest.Builder()
            .aggregate(DataType.TYPE_STEP_COUNT_DELTA, DataType.AGGREGATE_STEP_COUNT_DELTA)
            .aggregate(DataType.TYPE_DISTANCE_DELTA, DataType.AGGREGATE_DISTANCE_DELTA)
            .bucketByActivitySegment(5, TimeUnit.MINUTES)
            .setTimeRange(startDay(), endDay(), TimeUnit.MILLISECONDS)
            .enableServerQueries()
            .build()

    private fun endDay(): Long {
        val cal = Calendar.getInstance()
        val now = Date()
        cal.time = now
        return cal.timeInMillis
    }

    private fun startDay(): Long {
        val cal = Calendar.getInstance()
        val now = Date()
        cal.time = now
        cal.add(Calendar.DAY_OF_WEEK_IN_MONTH, -1)
        return  cal.timeInMillis
    }
}