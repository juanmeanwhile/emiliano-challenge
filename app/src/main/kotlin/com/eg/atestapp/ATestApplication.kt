package com.eg.atestapp

import android.app.Activity
import android.app.Application
import com.eg.atestapp.data.injection.DataModule
import com.eg.atestapp.injection.app.AppComponent
import com.eg.atestapp.injection.app.AppModule
import com.eg.atestapp.injection.app.DaggerAppComponent
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

class ATestApplication : Application(), HasActivityInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    private var app: ATestApplication? = null
    private lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()

        app = this
        initAdidasTestComponent()
        appComponent.inject(this)
    }

    private fun initAdidasTestComponent() {
        appComponent = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .dataModule(DataModule(this))
                .build()
    }

    override fun activityInjector(): DispatchingAndroidInjector<Activity> {
        return dispatchingAndroidInjector
    }
}