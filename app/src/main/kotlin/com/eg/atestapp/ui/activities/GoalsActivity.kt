package com.eg.atestapp.ui.activities

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.eg.atestapp.R
import com.eg.atestapp.injection.viewmodel.ViewModelFactory
import com.eg.atestapp.model.entities.Goal
import com.eg.atestapp.model.entities.Status
import com.eg.atestapp.ui.viewmodel.GoalsActivityViewModel
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_goals.*
import kotlinx.android.synthetic.main.activity_goals_footer.*
import javax.inject.Inject
import kotlin.math.roundToInt

class GoalsActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var goalsActivityViewModel: GoalsActivityViewModel

    companion object {
        const val EXTRA_SELECTED_GOAL_ID = "EXTRA_SELECTED_GOAL_ID"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_goals)

        initViewModel()
    }

    private fun initViewModel() {
        goalsActivityViewModel = ViewModelProviders.of(this, viewModelFactory)[GoalsActivityViewModel::class.java]

        goalsActivityViewModel.statusLivedata.observe(this, Observer { updateStatus() })

        goalsActivityViewModel.goalLiveData.observe(this, Observer { updateViews(it) })
        goalsActivityViewModel.getGoalData(intent.getIntExtra(EXTRA_SELECTED_GOAL_ID, 0))
    }

    private fun updateViews(goal: Goal) {
        vGoalTitle.text = goal.title
        vGoalDescription.text = goal.description
        vGoalUnlockPoints.text = getString(R.string.goals_activity_reward, goal.reward.points)
        vGoalProgressBar.progress = goalsActivityViewModel.getExperienceProgress(goal).roundToInt()
        if (goal.isComplete()) {
            vGoalProgressTitle.text = getString(R.string.goals_activity_progress_complete)
            vGoalTrophy.setImageDrawable(goalsActivityViewModel.getTrophyDrawable(baseContext))
            vGoalTrophy.visibility = View.VISIBLE
        } else {
            vGoalProgressTitle.text = getString(R.string.goals_activity_progress)
        }
    }

    private fun updateStatus() {
        when (goalsActivityViewModel.statusLivedata.value) {
            Status.SUCCESS -> showLoading(false)
            Status.ERROR -> showErrorToast()
            Status.LOADING -> showLoading(true)
        }
    }

    private fun showErrorToast() {
        Toast.makeText(this, R.string.something_went_wrong, Toast.LENGTH_SHORT).show()
        showLoading(false)
    }

    private fun showLoading(mustShow: Boolean) {
        vGoalsLoading.visibility = if (mustShow) View.VISIBLE else View.INVISIBLE
    }
}
