package com.eg.atestapp.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.eg.atestapp.R
import com.eg.atestapp.model.entities.Goal
import kotlinx.android.synthetic.main.item_goal.view.*

class GoalsAdapter(private var goalsList: MutableList<Goal>,
                   private val onClickInterface: GoalsAdapterClickListener)
    : RecyclerView.Adapter<GoalsAdapter.GoalViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): GoalViewHolder {
        val inflatedView = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.item_goal, viewGroup, false)
        return GoalViewHolder(inflatedView)
    }

    override fun onBindViewHolder(holder: GoalViewHolder, position: Int) {
        holder.bindGoalInfo(goalsList[position], position, onClickInterface)
    }

    override fun getItemCount(): Int {
        return goalsList.size
    }

    class GoalViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val goalTitle: TextView = view.vGoalItemTitle

        fun bindGoalInfo(goalInfo: Goal, position : Int,
                         onClickInterface: GoalsAdapterClickListener) {
            goalTitle.text = goalInfo.title
            itemView.setOnClickListener { onClickInterface.onClickGoal(position) } //TODO Juan: Memory allocation in the bind method, bad for performance

        }
    }
}