package com.eg.atestapp.ui.views

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatDialogFragment
import com.eg.atestapp.R
import kotlinx.android.synthetic.main.dialog_name_input.view.*


class NameInputDialog: AppCompatDialogFragment() {

    lateinit var mListener: NameInputDialogListener


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity!!)
        // Get the layout inflater
        val inflater = activity!!.layoutInflater

        val dialog = inflater.inflate(R.layout.dialog_name_input, null)

        builder.setView(dialog)
            // Add action buttons
            .setPositiveButton(R.string.dialog_ok) { _, _ ->
                dialog.vDialogText?.text?.let {
                    mListener.onNameDialogPositiveClick(it.toString())
                }
            }
            .setNegativeButton(R.string.dialog_cancel
            ) { _, _ -> this.dialog.cancel() }
        return builder.create()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            mListener = activity as NameInputDialogListener
        } catch (e: ClassCastException) {
            throw ClassCastException(activity!!.toString() + " must implement NameInputDialogListener")
        }

    }

    interface NameInputDialogListener {
        fun onNameDialogPositiveClick(newName : String)
    }

}