package com.eg.atestapp.ui.viewmodel

import android.content.Context
import android.graphics.drawable.Drawable
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.eg.atestapp.R
import com.eg.atestapp.model.entities.Goal
import com.eg.atestapp.model.entities.Status
import com.eg.atestapp.model.interactor.GoalsInteractor
import javax.inject.Inject

class GoalsActivityViewModel @Inject constructor(private val goalsInteractor: GoalsInteractor) : ViewModel() {

    var statusLivedata = MutableLiveData<Status>().apply { value = Status.LOADING }
    var goalLiveData = MutableLiveData<Goal>()

    fun getGoalData(goalID: Int) {
        //TODO Juan: everytime this is called gets a new data and observes it forever.  He should be using a Mediator

        goalsInteractor.getGoalByID(goalID).observeForever { resource ->
            when (resource.status) {
                Status.SUCCESS -> resource.data?.let { updateGoal(it) }
                Status.ERROR -> statusLivedata.postValue(Status.ERROR)
                Status.LOADING -> statusLivedata.postValue(Status.LOADING)
            }
        }
    }

    private fun updateGoal(goal: Goal) {
        goalLiveData.postValue(goal)
        statusLivedata.postValue(Status.SUCCESS)
    }

    fun getExperienceProgress(goal: Goal) = 100.0 / goal.goal * goal.progress

    /**
     *  Fast simple implementation of reward icon.
     */
    fun getTrophyDrawable(baseContext: Context): Drawable {
        val reward = goalLiveData.value!!.reward
        return when (reward.trophy) {
            "silver_medal" -> baseContext.getDrawable(R.drawable.ic_silver_medal)!! //TODO Juan: enum class for trophy type
            "bronze_medal" -> baseContext.getDrawable(R.drawable.ic_bronze_medal)!!
            "gold_medal" -> baseContext.getDrawable(R.drawable.ic_gold_medal)!!
            "zombie_hand" -> baseContext.getDrawable(R.drawable.ic_zombie_hand)!!
            else -> baseContext.getDrawable(R.drawable.ic_arrow)!!
        }

    }
}
