package com.eg.atestapp.ui.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.eg.atestapp.R
import com.eg.atestapp.injection.viewmodel.ViewModelFactory
import com.eg.atestapp.model.entities.Status
import com.eg.atestapp.ui.activities.GoalsActivity.Companion.EXTRA_SELECTED_GOAL_ID
import com.eg.atestapp.ui.adapter.GoalsAdapter
import com.eg.atestapp.ui.adapter.GoalsAdapterClickListener
import com.eg.atestapp.ui.viewmodel.MainActivityViewModel
import com.eg.atestapp.ui.views.NameInputDialog
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main_header.*
import javax.inject.Inject
import kotlin.math.roundToInt


class MainActivity : AppCompatActivity(), GoalsAdapterClickListener, NameInputDialog.NameInputDialogListener {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var mainActivityViewModel: MainActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //TODO init methods are appreciated
        initViewModel()
        initAdapter()
        initListeners()
    }

    private fun initListeners() {
        vUserName.setOnClickListener { showChangeNameDialog() }
    }

    private fun initAdapter() {
        vGoalsRecyclerView.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            adapter = GoalsAdapter(mainActivityViewModel.goalList, this@MainActivity)
        }
    }

    private fun initViewModel() {
        mainActivityViewModel = ViewModelProviders.of(this, viewModelFactory)[MainActivityViewModel::class.java]

        mainActivityViewModel.statusLivedata.observe(this, Observer { updateStatus() })
        mainActivityViewModel.userNameLiveData.observe(this, Observer { updateUsername(it)})
        mainActivityViewModel.userLevelNameLiveData.observe(this, Observer { updateUserLevel(it) })
        mainActivityViewModel.userProgressNameLiveData.observe(this, Observer { vUserProgress.progress = it.roundToInt() })

        mainActivityViewModel.initFitnessTracking(this)
    }

    private fun updateUserLevel(level : Int) {
        vUserLvl.text = getString(R.string.main_activity_lvl, level)
    }

    private fun updateUsername(username: String) {
        if (username.isNotEmpty()) { // This should be replaced by predefined name logic in the viewmodel
            vUserName.text = username
        }
    }

    private fun updateStatus() {
        when (mainActivityViewModel.statusLivedata.value) {
            Status.SUCCESS -> showGoalsList()
            Status.ERROR -> showErrorToast()
            Status.LOADING -> showLoading(true)
        }
    }

    override fun onClickGoal(selectedGoalPosition: Int) {
        startActivity(
            Intent(this, GoalsActivity::class.java).
                putExtra(EXTRA_SELECTED_GOAL_ID, mainActivityViewModel.goalList[selectedGoalPosition].id) //TODO Juan: could save the troubles of saving the
                // goals in ViewModel just returning the Goal instead of the position
        )
    }

    private fun showLoading(mustShow: Boolean) {
        vMainActivityLoadingBar.visibility = if (mustShow) View.VISIBLE else View.INVISIBLE
    }

    private fun showGoalsList() {
        showLoading(false)
        vGoalsRecyclerView.adapter?.notifyDataSetChanged()
        vGoalsRecyclerView.visibility = View.VISIBLE
    }

    private fun showErrorToast() {
        Toast.makeText(this, R.string.something_went_wrong, Toast.LENGTH_SHORT).show()
        showLoading(false)
    }

    private fun showChangeNameDialog() {
        NameInputDialog().show(supportFragmentManager, "Name Dialog")
    }

    override fun onNameDialogPositiveClick(newName: String) {
        mainActivityViewModel.updateName(newName)
    }
}
