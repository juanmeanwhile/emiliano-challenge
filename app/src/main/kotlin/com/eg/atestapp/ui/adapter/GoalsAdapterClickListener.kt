package com.eg.atestapp.ui.adapter

interface GoalsAdapterClickListener {

    fun onClickGoal(selectedGoalPosition : Int)
}