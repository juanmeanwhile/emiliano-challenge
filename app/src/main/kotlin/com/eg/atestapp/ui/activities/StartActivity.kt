package com.eg.atestapp.ui.activities

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.eg.atestapp.R
import com.eg.atestapp.util.FitOptionsHelper
import com.google.android.gms.auth.api.signin.GoogleSignIn
import dagger.android.AndroidInjection
import javax.inject.Inject
import com.google.android.gms.location.LocationServices


class StartActivity : AppCompatActivity() {

    @Inject // TODO This should be added into a start activity viewmodule
    lateinit var fitOptions: FitOptionsHelper

    companion object {
        private const val GOOGLE_FIT_PERMISSIONS_REQUEST_CODE = 7777
        private const val LOCATION_REQUEST_CODE = 7776
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(com.eg.atestapp.R.layout.activity_start)

        askForLocationPermission()
    }

    private fun askForLocationPermission() {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {

            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_REQUEST_CODE
            )
        } else {
            connectToFit()
        }
    }

    private fun connectToFit() {
        if (!GoogleSignIn.hasPermissions(GoogleSignIn.getLastSignedInAccount(this), fitOptions.options)) {
            GoogleSignIn.requestPermissions(
                this, // Activity
                GOOGLE_FIT_PERMISSIONS_REQUEST_CODE,
                GoogleSignIn.getLastSignedInAccount(this),
                fitOptions.options
            )
        } else {
            startMainApp()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                GOOGLE_FIT_PERMISSIONS_REQUEST_CODE -> startMainApp()
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            LOCATION_REQUEST_CODE ->
                if (resultGranted(grantResults)) {
                    connectToFit()
                } else {
                    finish()
                    Toast.makeText(this, R.string.location_required_message, Toast.LENGTH_LONG).show()
                    // TODO Offer a better treatment of permission cancel case
                }
        }
    }

    private fun resultGranted(grantResults: IntArray) =
        grantResults.size == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED

    private fun startMainApp() {
        startActivity(Intent(this, MainActivity::class.java))
    }
}
