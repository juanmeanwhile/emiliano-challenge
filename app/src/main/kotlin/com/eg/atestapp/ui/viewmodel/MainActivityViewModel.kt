package com.eg.atestapp.ui.viewmodel

import android.app.Activity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.eg.atestapp.model.entities.*
import com.eg.atestapp.model.interactor.GoalsInteractor
import com.eg.atestapp.model.interactor.UserInteractor
import com.eg.atestapp.util.AbsentLiveData
import com.eg.atestapp.util.FitHelper
import javax.inject.Inject

class MainActivityViewModel @Inject constructor(private val userInteractor: UserInteractor,
                                                private val goalsInteractor: GoalsInteractor,
                                                private val fitnesHelper: FitHelper) : ViewModel() {

    private var _goalsLiveData: LiveData<Resource<List<Goal>>> = goalsInteractor.getGoals()
    private var _userLiveData: LiveData<User> = userInteractor.getUser()

    var statusLivedata = MutableLiveData<Status>().apply { value = Status.LOADING }
    var userNameLiveData = MutableLiveData<String>().apply { value = "" }
    var userLevelNameLiveData = MutableLiveData<Int>().apply { value = 0 }
    var userProgressNameLiveData = MutableLiveData<Double>().apply { value = 0.0 }

    var goalList = mutableListOf<Goal>()  //TODO Juan: should not expose a mutableList but a List

    /**
     *  We have private livedata objects that back-up actual live-data consumed by the views.
     */
    init {
        observeGoals()
        observeUser()
    }

    private fun observeGoals() {
        _goalsLiveData.observeForever { resource ->
            when (resource.status) {
                Status.SUCCESS -> resource.data?.let { updateGoalList(it) }
                Status.ERROR -> statusLivedata.postValue(Status.ERROR)
                Status.LOADING -> statusLivedata.postValue(Status.LOADING)
            }
        }

        _goalsLiveData = goalsInteractor.getGoals()
    }

    private fun observeUser() {
        _userLiveData.observeForever {
            userNameLiveData.postValue(it.username)
            userLevelNameLiveData.postValue(it.level)
            userProgressNameLiveData.postValue(
                getLevelProgress(it.experience,
                    it.experienceRequiredForNextLevel))
        }
    }

    private fun updateGoalList(it: List<Goal>) {
            goalList.clear()
            goalList.addAll(it)  //TODO Juan: Why clear
            statusLivedata.postValue(Status.SUCCESS)
    }

    override fun onCleared() {
        //TODO Juan: Hacky way to remove observers, probabluy because he is using observer forever (instead of a Mediator)
        //TODO Juna: LiveData is already detached with the lifecycle, observingForever might be removing that advantage
        _goalsLiveData = AbsentLiveData.create()
        super.onCleared()
    }

    /**
     *  Calculates the progress bar based on current experience to lvl up
     */
    private fun getLevelProgress(experience: Int, experienceRequiredForNextLevel: Int)
             = 100.0 / experienceRequiredForNextLevel * experience

    fun updateName(newName: String) {
        userInteractor.setUserName(newName)
        userNameLiveData.postValue(newName)  //TODO Juan: bye single source of true :(
    }

    fun initFitnessTracking(activity : Activity) {
        fitnesHelper.subscribe(activity)
        fitnesHelper.getTodaySteps(activity).observeForever { resource ->
            if(resource.status == Status.SUCCESS) {
                resource.data?.let {
                    goalsInteractor.updateGoalStatus(it, GoalType.STEP)
                }
            }
        }

        fitnesHelper.getTodayWalkingDistance(activity).observeForever { resource ->
            if(resource.status == Status.SUCCESS) {
                resource.data?.let {
                    goalsInteractor.updateGoalStatus(it, GoalType.WALKING_DISTANCE)
                }
            }
        }

        fitnesHelper.getTodayRunningDistance(activity).observeForever { resource ->
            if(resource.status == Status.SUCCESS) {
                resource.data?.let {
                    goalsInteractor.updateGoalStatus(it, GoalType.RUNNING_DISTANCE)
                }
            }
        }
    }
}