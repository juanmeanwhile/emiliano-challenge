package com.eg.atestapp.injection.app

import android.app.Application
import com.eg.atestapp.util.FitHelper
import com.eg.atestapp.util.FitOptionsHelper
import com.google.android.gms.fitness.FitnessOptions
import com.google.android.gms.fitness.data.DataType
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class AppModule(internal val application : Application) {

    @Provides
    @Singleton
    fun provideFitHelper(): FitHelper {
        return FitHelper()
    }

    @Provides
    @Singleton // We could directly add here the options but is preferable to have a proper class contain the required args.
    fun provideFitOptionsHelper(): FitOptionsHelper {
        return FitOptionsHelper()
    }
}