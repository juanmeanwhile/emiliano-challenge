package com.eg.atestapp.injection.app

import com.eg.atestapp.ATestApplication
import com.eg.atestapp.data.injection.DataModule
import com.eg.atestapp.injection.AndroidBindingModule
import com.eg.atestapp.injection.viewmodel.ViewModelModule
import com.eg.atestapp.ui.activities.GoalsActivity
import com.eg.atestapp.ui.activities.MainActivity
import com.eg.atestapp.ui.activities.StartActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, ViewModelModule::class, AndroidBindingModule::class, DataModule::class])
interface AppComponent {

    fun inject(adidasTestApplication: ATestApplication)

    fun inject(mainActivity : MainActivity)

    fun inject(goalsActivity: GoalsActivity)

    fun inject(startActivity: StartActivity)
}