package com.eg.atestapp.injection.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.eg.atestapp.ui.viewmodel.GoalsActivityViewModel
import com.eg.atestapp.ui.viewmodel.MainActivityViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
internal abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(appViewModelFactory: ViewModelFactory): ViewModelProvider.Factory


    @Binds
    @IntoMap
    @ViewModelKey(MainActivityViewModel::class)
    internal abstract fun bindMainActivityViewModel(mainActivityViewModel: MainActivityViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(GoalsActivityViewModel::class)
    internal abstract fun bindGoalsDetailActivityViewModel(favoritesActivityViewModel: GoalsActivityViewModel): ViewModel

}
