package com.eg.atestapp.injection

import com.eg.atestapp.ui.activities.GoalsActivity
import com.eg.atestapp.ui.activities.MainActivity
import com.eg.atestapp.ui.activities.StartActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class AndroidBindingModule {

    @ContributesAndroidInjector
    internal abstract fun contributesMainActivity(): MainActivity

    @ContributesAndroidInjector
    internal abstract fun contributesGoalsActivity(): GoalsActivity

    @ContributesAndroidInjector
    internal abstract fun contributesStartActivity(): StartActivity
}


