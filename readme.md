Hi! First of all thank you for reviewing my code!
This is a small explanation upon my decisions and ideas.

#### The Project

The project is a sport goals tracker with a level up system. As you unlock trophies you gain experience and with it, you level up.

#### Decisions
I used Android X / Jetpack to boost the speed of development, and use of the architecture component.

The architecture is MVVMi, with repository to have an unique point of access to information.
I added an interactor to access the repositories data.

I separated the work in 2 modules, app and datamodel.
I could have further separated the data from the model (its actually separated in 2 different packages but not modules).

Important to note, for development i used build flavors (mock, full) for mocked repositories and real apis.
When you do this you have to be aware that the actual package name changes noting this in the signing keys configuration under google-api's console. At end of the development still, i moved the implementations into the test folder for further usage.

The dependencies are defined on dependencies.gradle.
This way we can define app, model and data dependencies in a unique place, in case they share libs (like dagger). Another approach is to use de.fayard.buildSrcVersions, that does a great job keeping a clean library of dependencies (i would use this path if i had to redo-it).

Shared preference for User.
At first glance i thought it was the best decision, as i would only save username + experience. On a second thought Room + Livedata would let me update the user lvl / status directly without manually getting the new data. This can be seen on user lvl happening only on the creation of viewmodel. (I would move it into a room/livedata implementation if i had more time)

Also, ideally Data layer should not know anything about android, but i did an exception through the use of LiveData. Android officially encourages the use of the object as the observer result of a repository call. Thanksfully you can add the dependency of just livedata, without the need of adding the whole android dependencies.

I dint use navigation components as they are designed for apps that have one main activity with multiple Fragment destinations. 

I didnt use databinding neither rxjava/kotlin. Still the observable/livedata design makes the app reactive to changes.

Another thing i would work in if needed, is in creating a viewmodel for the recycler view.

I used static app executors to divide work in different threads aside the main thread. 

I would have loved to spend more time on tests. I can submit a second version with tests if it will make the difference :)

#### Regarding the process
Was quite fun to play with google fit. The api changed a lot in the last years and even some of their own documentation is outdated, so i had to play a bit with it to completely understand it.

I think i will continue using my app to track some other goals.

If you want to test it without actually going for a run you can add results directly into Google Fit's app. This will be reflected on the test app inmediatly :)

Best,
Emi.
